# POO . 2.Contrasena

Ejercicio 2

2. Haz una clase llamada Password que siga las siguientes condiciones:
• Que tenga los atributos longitud y contraseña . Por defecto, la longitud será de 8.
• Los constructores serán los siguiente:
- Un constructor por defecto.
- Un constructor con la longitud que nosotros le pasemos. Generara una
contraseña aleatoria con esa longitud.