package dto;


import java.util.Random;
import java.util.Scanner;

public class Password {

	private int longitud;
	private String contrasena;
	
	
	public Password() {
		
		this.longitud=8;
		this.contrasena=generarcontrasena(this.longitud);
		
	}
	
	

	public Password(int longitud) {
		this.longitud = longitud;
		this.contrasena = generarcontrasena(this.longitud);
	}



	public String generarcontrasena(int longitud) {

		char[] ch = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
	        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
	        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	        'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
	        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	        'w', 'x', 'y', 'z' };
	    
	    char[] pass=new char[longitud];
	    Random random=new Random();
	    for (int i = 0; i < longitud; i++) {
	      pass[i]=ch[random.nextInt(ch.length)];
	    }
	    
	    return new String(pass);
		
	}
	
	
	public int getLongitud() {
		return longitud;
	}
	
	public String getContrasena() {
		return contrasena = contrasena;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	//MOSTRAR PASSWORD
	@Override
	public String toString() {
		return "Password:" + contrasena;
	}	
	
	
	
	
}
